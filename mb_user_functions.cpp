#include "mb_user_functions.h"

#include <stdint.h>
#include "version.h"
#include "modbus_rtu_stm32l0.h"
#include "stm32_nvm.h"
#include "mdt01.h"
#include "stm32l051xx.h"
#include "stm32_nvm.h"
#include "globals.h"
#include "modbus.h"
#include "string.h"
#include "globals.h"

eMBException function100(uint8_t *pPdu, uint8_t *pLength)
{
    switch (pPdu[1]) {
    //    case MD_CMD_COMMON_SET_CONNECTION_PARAM_BY_ADR: {
    //            if (*pLength != 8)
    //                return (eMBException)MDB_EX_COMMAND_SIZE;

    //            if (pPduIn[2] < 1 || pPduIn[2] > 247)
    //                return (eMBException)MD_EX_INVALID_COMMAND_DATA_VALUES;

    //            pPduOut[0] = 65;
    //            pPduOut[1] = MD_CMD_COMMON_SET_CONNECTION_PARAM_BY_ADR;

    //            SCommonSetConnectionParams __packed *pParam = (SCommonSetConnectionParams *)&pPduOut[2];

    //            nvmUnlockPECR();

    //            *(uint8_t *)(DATA_EEPROM_BASE) = pParam->address;
    //            __packed_UINT32_WRITE(DATA_EEPROM_BASE + 1, pParam->baud);
    //            *(uint8_t *)(DATA_EEPROM_BASE + 5) = pParam->parity;
    //            *(uint16_t *)(DATA_EEPROM_BASE + 6) = CRC16(pParam, 6);

    //            nvmLockPECR();

    //            if (CRC16((uint8_t *)DATA_EEPROM_BASE, 8))
    //                return (eMBException)MD_EX_INVALID_WRITE_TO_EEPROM;

    //            *pLength = 2;
    //        }
    //        break;
    case MDB_CMD_GET_DEVID: {
            if (*pLength != 2)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            MdbCmdReadId __packed *pIdent = (MdbCmdReadId *)&pPdu[2];

            //pIdent->productId = 0x0100;
            pIdent->pid = 0x01;

            pIdent->mode = 1;

            pIdent->appMajor = VER_MAJOR;
            pIdent->appMinor = VER_MINOR;
            pIdent->appPatch = VER_PATCH;
            pIdent->appBuild = VER_BUILD;

            pIdent->bootMajor = 0;
            pIdent->bootMinor = 0;
            pIdent->bootPatch = 0;
            pIdent->bootBuild = 0;

            *pLength = sizeof(MdbCmdReadId) + 2;
        }
        break;

    case MDB_CMD_RESET: {
            if (*pLength != 2)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 2;
        }
        break;

    default:
        return (eMBException)MDB_EX_COMMAND_CODE;
    }

    return MB_EX_NONE;
}

eMBException function102(uint8_t *pPdu, uint8_t *pLength)
{
    if (*(uint16_t *)(pPdu + 1) != MDB_PRODUCT_CODE_T01)
        return (eMBException)MDB_EX_PID;

    switch (pPdu[3]) {
    case MD_T01_CMD_WR_SERIAL: {
            if (*pLength != 4 + 16)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4;

            memcpy(&gEeprom.serial.serial, &pPdu[4], 16);
            gEeprom.serial.crc = crc16(&gEeprom.serial, sizeof(gEeprom.serial.serial));

            if (nvmEepromWrite((uint32_t)&gEeprom.serial - (uint32_t)&gEeprom, &gEeprom.serial, sizeof(gEeprom.serial)) != NVM_OK) {
                gStatus0.errLoadSerial = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadSerial = 0;
        }
        break;
    case MD_T01_CMD_WR_DATE_MANUFACTURE: {
            if (*pLength != 4 + 4)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4;

            MdT01Date __packed *pDate = (MdT01Date *)&pPdu[4];

            gEeprom.dateManufacture.day = pDate->day;
            gEeprom.dateManufacture.month = pDate->month;
            gEeprom.dateManufacture.year = pDate->year;
            gEeprom.dateManufacture.crc = crc16(&gEeprom.dateManufacture, sizeof(gEeprom.dateManufacture) - 2);

            if (nvmEepromWrite((uint32_t)&gEeprom.dateManufacture - (uint32_t)&gEeprom, &gEeprom.dateManufacture, sizeof(gEeprom.dateManufacture)) != NVM_OK) {
                gStatus0.errLoadDateManufacture = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadDateManufacture = 0;
        }
        break;
    case MD_T01_CMD_WR_DATE_VERIFICATION: {
            if (*pLength != 4 + 4)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4;

            MdT01Date __packed *pDate = (MdT01Date *)&pPdu[4];

            gEeprom.dateVerification.day = pDate->day;
            gEeprom.dateVerification.month = pDate->month;
            gEeprom.dateVerification.year = pDate->year;
            gEeprom.dateVerification.crc = crc16(&gEeprom.dateVerification, sizeof(gEeprom.dateVerification) - 2);

            if (nvmEepromWrite((uint32_t)&gEeprom.dateVerification - (uint32_t)&gEeprom, &gEeprom.dateVerification, sizeof(gEeprom.dateVerification)) != NVM_OK) {
                gStatus0.errLoadDateVerification = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadDateVerification = 0;
        }
        break;
    case MD_T01_CMD_WR_CORRECTIONS: {
            if (*pLength != 4 + 48)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4;

            MdT01Corrections __packed *pCor = (MdT01Corrections *)&pPdu[4];

            for (uint8_t i = 0; i < 16; i++) {
                gEeprom.correctionTable.temperature[i] = pCor->temp[i];
                gEeprom.correctionTable.correction[i] = pCor->cor[i];
            }
            gEeprom.correctionTable.crc = crc16(&gEeprom.correctionTable, sizeof(gEeprom.correctionTable) - 2);

            if (nvmEepromWrite((uint32_t)&gEeprom.correctionTable - (uint32_t)&gEeprom, &gEeprom.correctionTable, sizeof(gEeprom.correctionTable)) != NVM_OK) {
                gStatus0.errLoadCorrectionTable = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadCorrectionTable = 0;
        }
        break;
    case MD_T01_CMD_RD_CORRECTIONS: {
            if (*pLength != 4)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4 + 48;

            MdT01Corrections __packed *pCor = (MdT01Corrections *)&pPdu[4];

            for (uint8_t i = 0; i < 16; i++) {
                pCor->temp[i] = gEeprom.correctionTable.temperature[i];
                pCor->cor[i] = gEeprom.correctionTable.correction[i];
            }
        }
        break;
    case MD_T01_CMD_RD_LOG: {
            if (*pLength != 4)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            *pLength = 4 + 1 + 192;

            memcpy(&pPdu[4], &gTemperatureLog, sizeof(MdT01Log));
        }
        break;
    case MD_T01_CMD_ER_UPTIME: {
            if (*pLength != 4)
                return (eMBException)MDB_EX_COMMAND_SIZE;

            uint32_t address = (uint32_t)UPTIME_BLOCK_BASE;
            int32_t size = 64 * UPTIME_BLOCK_COUNT;
            while (size > 0) {
                nvmErasePage(address);
                address += 128;
                size -= 128;
            }
        }
        break;
    default:
        return (eMBException)MDB_EX_COMMAND_CODE;
    }

    return MB_EX_NONE;
}
