#pragma once
#include "stdint.h"
#include "modbus_rtu_stm32l0.h"
#include "tmp116.h"
#include "mdt01.h"
#include "stm32_nvm.h"

#ifdef _GLOBALS_CPP
    #define GLOBALS_PREFIX
#else
    #define GLOBALS_PREFIX extern
#endif

#define UPTIME_BLOCK_BASE   0x0800C000
#define UPTIME_BLOCK_COUNT  3
struct Uptime {
    struct UptimeBlock {
        struct Data {
            uint32_t idx;
            uint32_t uptime;
            uint32_t sum;
        } items[5];

        uint32_t reserved;
    } block;

    uint8_t  idxBlock;
    uint8_t  idxItem;
};
GLOBALS_PREFIX Uptime           gUptime;

GLOBALS_PREFIX MdT01Status0     gStatus0;

GLOBALS_PREFIX MdT01Log         gTemperatureLog;

GLOBALS_PREFIX uint16_t         gVcc;
GLOBALS_PREFIX int16_t          gTmpCpu;
GLOBALS_PREFIX int16_t          gTmpSensor;
GLOBALS_PREFIX int16_t          gTmpCorrected;
GLOBALS_PREFIX int16_t          gTmpAverage;

GLOBALS_PREFIX uint32_t         gUptimeSession;
GLOBALS_PREFIX uint32_t         gUptimeLife;
GLOBALS_PREFIX uint32_t         gStartupCount;

GLOBALS_PREFIX nsTmp116::Tmp116 gTmp116;

//==================================================================================================
// EEPROM Store
//==================================================================================================

struct __attribute__((packed)) sModbusConfig {
    uint8_t address;
    uint8_t baudrate;
    uint8_t parity;
    uint16_t crc;
};

struct __attribute__((packed)) sMeasureConfig {
    uint8_t period;
    uint8_t sampleSize;
    uint16_t reserved[6];
    uint16_t crc;
};

struct __attribute__((packed)) sDate {
    uint8_t day;
    uint8_t month;
    uint16_t year;
    uint16_t crc;
};

struct __attribute__((packed)) sSerial {
    uint8_t serial[16];
    uint16_t crc;
};

struct __attribute__((packed)) sCorrections {
    int16_t temperature[16];
    int8_t correction[16];
    uint16_t crc;
};

struct __attribute__((packed)) sEeprom {
    sModbusConfig       modbus;
    sMeasureConfig      measure;
    sDate               dateManufacture;
    sDate               dateVerification;
    sSerial             serial;
    sCorrections        correctionTable;
} GLOBALS_PREFIX gEeprom;
