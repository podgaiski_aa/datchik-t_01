#include "modbus.h"
#include "globals.h"
#include "stm32l051xx.h"
#include "intrinsics.h"

uint16_t INPUT_REGION[][2] = {
    {0x0000, 0x000C},
    {0x0020, 0x0025},
    {0xFFFF, 0xFFFF}
};

eMBException extModbusInputs(uint16_t *pRegBuffer, uint16_t address, uint8_t count)
{
    uint32_t lCount = count;
    if (checkRegsRegion(INPUT_REGION, address, lCount) == false)
        return MB_EX_ILLEGAL_DATA_ADDRESS;
        
    uint16_t __packed *pRegs = pRegBuffer;
    
    switch (address) {
    case 0x0000:
        *pRegs++ = __REVSH((gUptimeSession & 0xFFFF0000) >> 16);
        if (--count == 0)
            break;
    case 0x0001:
        *pRegs++ = __REVSH(gUptimeSession & 0xFFFF);
        if (--count == 0)
            break;
    case 0x0002:
        *pRegs++ = __REVSH((gUptimeLife & 0xFFFF0000) >> 16);
        if (--count == 0)
            break;
    case 0x0003:
        *pRegs++ = __REVSH(gUptimeLife & 0xFFFF);
        if (--count == 0)
            if (--count == 0)
                break;
    case 0x0004:
        *pRegs++ = __REVSH((gStartupCount & 0xFFFF0000) >> 16);
        if (--count == 0)
            break;
    case 0x0005:
        *pRegs++ = __REVSH(gStartupCount & 0xFFFF);
        if (--count == 0)
            if (--count == 0)
                break;
    case 0x0006:
        *pRegs++ = __REVSH(gEeprom.dateManufacture.day);
        if (--count == 0)
            break;
    case 0x0007:
        *pRegs++ = __REVSH(gEeprom.dateManufacture.month);
        if (--count == 0)
            break;
    case 0x0008:
        *pRegs++ = __REVSH(gEeprom.dateManufacture.year);
        if (--count == 0)
            break;
    case 0x0009:
        *pRegs++ = __REVSH(gEeprom.dateVerification.day);
        if (--count == 0)
            break;
    case 0x000A:
        *pRegs++ = __REVSH(gEeprom.dateVerification.month);
        if (--count == 0)
            break;
    case 0x000B:
        *pRegs++ = __REVSH(gEeprom.dateVerification.year);
        if (--count == 0)
            break;
    case 0x000C:
        *pRegs++ = __REVSH(gStatus0.value);
        break;


    case 0x0020:
        *pRegs++ = __REVSH(gStatus0.value);
        if (--count == 0)
            break;
    case 0x0021:
        *pRegs++ = __REVSH(gTmpSensor);
        if (--count == 0)
            break;
    case 0x0022:
        *pRegs++ = __REVSH(gTmpCorrected);
        if (--count == 0)
            break;
    case 0x0023:
        *pRegs++ = __REVSH(gTmpAverage);
        if (--count == 0)
            break;
    case 0x0024:
        *pRegs++ = __REVSH(gTmpCpu);
        if (--count == 0)
            break;
    case 0x0025:
        *pRegs++ = __REVSH(gVcc);
        break;
    default:
        break;
    }

    return MB_EX_NONE;
}
