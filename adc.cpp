#include "adc.h"

#include "macro_common.h"
#include "stm32l051xx.h"

#define VREFINT_CAL_VREF                   ((uint32_t) 3000U)
#define VREFINT_CAL_ADDR                   ((uint16_t*) ((uint32_t)0x1FF80078U))
#define TEMPSENSOR_CAL1_ADDR               ((uint16_t*) ((uint32_t)0x1FF8007AU))
#define TEMPSENSOR_CAL2_ADDR               ((uint16_t*) ((uint32_t)0x1FF8007EU))

static uint16_t adcResult[3];

void adcInit()
{
    // ADC

    //LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_0, LL_GPIO_MODE_ANALOG);

    RCC->APB2ENR |= RCC_APB2ENR_ADCEN;

    SRF(ADC1->CFGR1, ADC_CFGR1_DMACFG, 1);
    SRF(ADC1->CFGR2, ADC_CFGR2_CKMODE, 2);
    ADC1->CFGR1 |= ADC_CFGR1_DMAEN;
    ADC->CCR |= ADC_CCR_LFMEN | ADC_CCR_VREFEN | ADC_CCR_TSEN;
    ADC1->CHSELR |= ADC_CHSELR_CHSEL0 | ADC_CHSELR_CHSEL17 | ADC_CHSELR_CHSEL18;
    ADC1->SMPR = 7;
    ADC1->IER |= ADC_IER_EOCALIE;

    NVIC_EnableIRQ(ADC1_COMP_IRQn);

    // DMA

    SRF(DMA1_CSELR->CSELR, DMA_CSELR_C1S, 0);
    SRF(DMA1_Channel1->CCR, DMA_CCR_PSIZE, 1);
    SRF(DMA1_Channel1->CCR, DMA_CCR_MSIZE, 1);
    SRF(DMA1_Channel1->CPAR,  DMA_CPAR_PA, (uint32_t)&ADC1->DR);
    SRF(DMA1_Channel1->CMAR,  DMA_CMAR_MA, (uint32_t)adcResult);
    DMA1_Channel1->CNDTR = 3;
    DMA1_Channel1->CCR |=  DMA_CCR_MINC | DMA_CCR_CIRC | DMA_CCR_EN;
}

void adcStartConversion()
{
    if (ADC1->CR & ADC_CR_ADEN)
        ADC1->CR |= ADC_CR_ADDIS;

    ADC1->CFGR1 &= ~ADC_CFGR1_DMAEN;

    ADC1->CR |= ADC_CR_ADCAL;
}

uint16_t getVcc()
{
    uint32_t val = (VREFINT_CAL_VREF * *VREFINT_CAL_ADDR * (uint64_t)adcResult[0] / (adcResult[1] * 4095)) * 10;
    return val;
}

int16_t getTmp()
{
    float tmp = (100. / (*TEMPSENSOR_CAL2_ADDR - *TEMPSENSOR_CAL1_ADDR))
                * (((float) * VREFINT_CAL_ADDR / (float)adcResult[1]) * (float)adcResult[2] - *TEMPSENSOR_CAL1_ADDR)
                + 30;

    return (int16_t) tmp * 100;
}

extern "C" __irq void ADC1_COMP_IRQHandler()
{
    if (ADC1->ISR & ADC_ISR_EOCAL) {
        ADC1->ISR = ADC_ISR_EOCAL;

        ADC1->CFGR1 |= ADC_CFGR1_DMAEN;
        ADC1->CR |= ADC_CR_ADEN | ADC_CR_ADSTART;
    }
}
