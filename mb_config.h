#pragma once

//------------------------------------------------------------------------------

#define MB_FUNC_HANDLERS_MAX                    ( 16 )
#define MB_FUNC_OTHER_REP_SLAVEID_BUF           ( 32 )

//------------------------------------------------------------------------------
// Enabling public functions

#define MB_FUNC_READ_DISCRETES_ENABLED          ( 0 )
#define MB_FUNC_READ_INPUTS_ENABLED             ( 1 )
#define MB_FUNC_READ_HOLDINGS_ENABLED           ( 1 )
#define MB_FUNC_READ_COILS_ENABLED              ( 0 )

#define MB_FUNC_WRITE_SINGLE_HOLDING_ENABLED    ( 1 )
#define MB_FUNC_WRITE_SINGLE_COIL_ENABLED       ( 0 )
#define MB_FUNC_WRITE_MULTIPLE_HOLDINGS_ENABLED ( 1 )
#define MB_FUNC_WRITE_MULTIPLE_COILS_ENABLED    ( 0 )

#define MB_FUNC_READWRITE_HOLDINGS_ENABLED      ( 0 )

#define MB_FUNC_EIT_ENABLED                     ( 1 )

//------------------------------------------------------------------------------
