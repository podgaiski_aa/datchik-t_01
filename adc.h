#pragma once
#include "stdint.h"

void adcInit() __attribute__((always_inline));
void adcStartConversion() __attribute__((always_inline));
uint16_t getVcc() __attribute__((always_inline));
int16_t getTmp() __attribute__((always_inline));
