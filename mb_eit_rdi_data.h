#pragma once

#include "globals.h"

#define EXTENDED_SIZE 3
#define MAX_OBJECT_LENGTH 32

const char *const basic[3] = {
    // VendorName
    "��� \"�����������\"",
    
    // ProductCode
    "����.405226.002",
    
    // MajorMinorRevision
    "1.0"
};

const char *const regular[4] = {
    // VendorUrl
    "http://www.omskmeteo.com",
    
    // ProductName
    "������ �-01",
    
    // ModelName
    "�-01",
    
    // UserApplicationName
    "�-01"
};

const char *const extended[EXTENDED_SIZE] = {
    // 0x80 SW ver
    "1.0",
    
    // 0x81 SW CRC
    "EE592AB7",
    
    // 0x82 Serial
    (const char *) &gEeprom.serial
};

