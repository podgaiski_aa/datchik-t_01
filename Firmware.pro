TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DEFINES += STM32L051xx

INCLUDEPATH += st \
    ../../Libs \
    ../../Libs/modbus \
    ../../Libs/devs \
    ../../Libs/sensor_tmp \
    ../../Libs/STM32L0xx/inc

SOURCES += \
        main.cpp \
    adc.cpp \
    globals.cpp \
    holdings.cpp \
    inputs.cpp \
    main.cpp \
    mb_user_functions.cpp \
    ../../Libs/stm32l0_i2c1.cpp \
    ../../Libs/stm32l0_nvm.cpp \
    ../../Libs/sensor_tmp/tmp116.cpp \
    ../../Libs/modbus/modbus.cpp \
    ../../Libs/modbus/modbus_rtu_stm32l0.cpp \
    ../../Libs/stm32_nvm.cpp \
    ../../Libs/devs/QMdBase.cpp \
    ../../Libs/devs/QMdT01.cpp

HEADERS += \
    adc.h \
    globals.h \
    mb_config.h \
    mb_eit_rdi_data.h \
    mb_user_alias.h \
    mb_user_functions.h \
    version.h \
    ../../Libs/devs/MdBase.h \
    ../../Libs/devs/MdTc01.h \
    ../../Libs/macro_common.h \
    ../../Libs/stm32l0_i2c1.h \
    ../../Libs/stm32l0_nvm.h \
    ../../Libs/sensor_tmp/tmp116.h \
    ../../Libs/modbus/modbus.h \
    ../../Libs/devs/MdDeviceCode.h \
    ../../Libs/stm32_nvm.h \
    ../../Libs/STM32L0xx/inc/stm32l0xx.h \
    ../../Libs/STM32L0xx/inc/stm32l051xx.h \
    ../../Libs/devs/MdT01.h \
    ../../Libs/devs/MdBase.h \
    ../../Libs/devs/MdProductCode.h \
    ../../Libs/devs/MdT01.h \
    ../../Libs/devs/QMdBase.h \
    ../../Libs/devs/QMdT01.h

DISTFILES += \
    st/startup_stm32l051xx.s \
    st/stm32l051xx_flash.icf \
    st/stm32l051xx_sram.icf \
    FreeRTOS/portasm.s
