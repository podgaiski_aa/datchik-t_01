#include "modbus_rtu_stm32l0.h"
#include "globals.h"
#include "stm32l0xx.h"
#include "adc.h"
#include "freertos.h"
#include "semphr.h"
#include "task.h"
#include "macro_common.h"
#include "string.h"
#include "mdt01.h"
#include "stm32_nvm.h"

// Функция инициализации сторожевого таймера IWDG
void Init_IWDG(uint32_t tw) // Параметр tw от 7мс до 26200мс
{
    /*
    // Для IWDG_PR=7 Tmin=6,4мс RLR=Tмс*40/256
    IWDG->KR = 0x5555;          // Ключ для доступа к таймеру
    IWDG->PR = 7;               // Обновление IWDG_PR
    IWDG->RLR = tw * 40 / 256;  // Загрузить регистр перезагрузки
    IWDG->KR = 0xAAAA;          // Перезагрузка
    IWDG->KR = 0xCCCC;          // Пуск таймера
    */
}

// Функция перезагрузки сторожевого таймера IWDG
void IWDG_res(void)
{
    IWDG->KR = 0xAAAA; // Перезагрузка
}

volatile int32_t cpuTmp[10] = {0, 0, 0, 0, 0};
volatile uint8_t cpuIdx = 0;

volatile int16_t sensorTmp[10];
volatile int8_t  sensorCor[10];
volatile uint8_t sensorIdx    = 0;
volatile int32_t sensorAvg    = 0;
volatile uint8_t sensorAvgCnt = 0;

volatile uint8_t  gInit;

int stAdc;
int stMb;
int stAdt;
int hp;

extern int __initial_sp;

StaticSemaphore_t sbModbus;
StaticSemaphore_t sbTmp;

#define stackSizeAdc    96
#define stackSizeModbus 96
#define stackSizeTmp    96

StackType_t  stackAdc[stackSizeAdc];
StackType_t  stackModbus[stackSizeModbus];
StackType_t  stackTmp[stackSizeTmp];

StaticTask_t tcbAdc;
StaticTask_t tcbModbus;
StaticTask_t tcbTmp;

void shutdown()
{

}

void uptimeInit()
{
    memset(&gUptime, 0, sizeof(gUptime));
    uint32_t maxIdx = 0;
    uint32_t uptime = 0;

    Uptime::UptimeBlock *pBlock = (Uptime::UptimeBlock *)UPTIME_BLOCK_BASE;
    
    // Поиск итема с максимальным индексом на всех страницах
    for (uint8_t block = 0; block < UPTIME_BLOCK_COUNT; block++) {
        for (uint8_t item = 0; item < 5; item++) {
            if (maxIdx < pBlock[block].items[item].idx) {
                maxIdx = pBlock[block].items[item].idx;
                uptime = pBlock[block].items[item].uptime;
                gUptime.idxBlock = block;
                gUptime.idxItem = item;
            }
        }
    }

    gUptimeLife = uptime;
    
    if ((gUptime.idxBlock & 0x1) && (gUptime.idxItem == 4)) {
        uint32_t addr;
        if (gUptime.idxBlock + 1 == UPTIME_BLOCK_COUNT)
            addr = UPTIME_BLOCK_BASE;
        else
            addr = ((uint32_t) & ((Uptime::UptimeBlock *)UPTIME_BLOCK_BASE)[gUptime.idxBlock + 1]);
        nvmErasePage(addr);
    }
}

void uptimeSave()
{
    Uptime::UptimeBlock *pBlock = (Uptime::UptimeBlock *)UPTIME_BLOCK_BASE;
    uint32_t curIdx = pBlock[gUptime.idxBlock].items[gUptime.idxItem].idx;

    gUptime.idxItem++;
    if (gUptime.idxItem == 5) {
        gUptime.idxItem = 0;
        gUptime.idxBlock++;
    }

    if (gUptime.idxBlock == UPTIME_BLOCK_COUNT)
        gUptime.idxBlock = 0;


    gUptime.block.items[gUptime.idxItem].idx = curIdx + 1;
    gUptime.block.items[gUptime.idxItem].uptime = gUptimeLife;
    gUptime.block.items[gUptime.idxItem].sum =
        0 -
        gUptime.block.items[gUptime.idxItem].idx -
        gUptime.block.items[gUptime.idxItem].uptime;

    nvmFlashWrite((uint32_t *)&pBlock[gUptime.idxBlock], (uint32_t *)&gUptime.block, 64);
}

void taskUptimeAdc(void *param)
{
    adcInit();

    uint32_t tick = xTaskGetTickCount();

    while (1) {
        adcStartConversion();
        vTaskDelayUntil(&tick, 1000);

        IWDG_res();

        uint16_t vcc = getVcc();
        int32_t  tmp = getTmp();

        cpuTmp[cpuIdx++] = tmp;
        if (cpuIdx == 5)
            cpuIdx = 0;

        int32_t sum = 0;

        for (uint8_t i = 0; i < 5; i++)
            sum += cpuTmp[i];

        tmp = sum / 5;

        taskENTER_CRITICAL();
        gUptimeSession++;
        gUptimeLife++;
        gVcc = vcc;
        gTmpCpu = tmp;
        taskEXIT_CRITICAL();

        //stAdc = uxTaskGetStackHighWaterMark(NULL);
    }
}

void taskCommunication(void *param)
{
    bool validate = false;

    BLK_START {
        if(crc16(&gEeprom.modbus, sizeof(gEeprom.modbus)))
            break;
        
        if (gEeprom.modbus.address < 1 || gEeprom.modbus.address > 247)
            break;

        if (gEeprom.modbus.baudrate > MDB_BAUD_115200)
            break;

        if (gEeprom.modbus.parity > 2)
            break;

        validate = true;
    } BLK_END;

    uint32_t baud;
    switch (gEeprom.modbus.baudrate) {
    case MDB_BAUD_300:
        baud =   300;
        break;
    case MDB_BAUD_600:
        baud =   600;
        break;
    case MDB_BAUD_1200:
        baud =   1200;
        break;
    case MDB_BAUD_2400:
        baud =   2400;
        break;
    case MDB_BAUD_4800:
        baud =   4800;
        break;
    case MDB_BAUD_9600:
        baud =   9600;
        break;
    case MDB_BAUD_19200:
        baud =   19200;
        break;
    case MDB_BAUD_38400:
        baud =   38400;
        break;
    case MDB_BAUD_57600:
        baud =   57600;
        break;
    case MDB_BAUD_115200:
        baud =   115200;
        break;
    default:
        baud = 9600;
    }

    gModbus.init();

    if (validate) {
        gModbus.setAddress(gEeprom.modbus.address);
        gModbus.setConnectionParams(baud, gEeprom.modbus.parity);
    } else {
        gModbus.setAddress(0x50);
        gModbus.setConnectionParams(9600, 2);
    }

    SemaphoreHandle_t hSemaphore = xSemaphoreCreateBinaryStatic(&sbModbus);

#ifdef USE_FREERTOS
    gModbus.setSemaphore(hSemaphore);
#endif
    gModbus.pShutdown = shutdown;

    gModbus.usart1_rx();
    gModbus.usart2_rx();

    while (1) {
        gModbus.loop();
        //stMb = uxTaskGetStackHighWaterMark(NULL);
#ifdef USE_FREERTOS
        xSemaphoreTake(gModbus.getSemaphore(), portMAX_DELAY);
#endif
    }
}

void taskSensor(void *param)
{
    SemaphoreHandle_t hSemaphore = xSemaphoreCreateBinaryStatic(&sbTmp);

    I2C1_init();

    gTmp116.setAddress(0x49);
    gTmp116.pTransfer = I2C1_transfer;

    gTmp116.m_configuration.asShort = 0;
    gTmp116.m_configuration.asValues.MOD = 3;
    gTmp116.m_configuration.asValues.AVG = 1;

    gTmp116.setSemaphore(hSemaphore);

    if (gStatus0.errLoadMeasure)
        gEeprom.measure.sampleSize = 5;

    for (uint8_t i = 0; i < gEeprom.measure.sampleSize; i++)
        sensorTmp[i] = 0;

    uint32_t tick = xTaskGetTickCount();

    while (1) {
        gTmp116.writeConfiguration();
        xSemaphoreTake(hSemaphore, portMAX_DELAY);

        if (gEeprom.measure.period)
            vTaskDelayUntil(&tick, gEeprom.measure.period * 1000);
        else
            vTaskDelayUntil(&tick, 1000);

        gTmp116.readTemperature();
        xSemaphoreTake(hSemaphore, portMAX_DELAY);

        gTmp116.processResult();

        sensorTmp[sensorIdx] = (int32_t)gTmp116.m_temperature * 100 / 128;

        // -------------------------------------------------------------------------------------------------------------
        // Вычисление поправки

        uint8_t iMax = 0xFF;
        for (int i = 0; i < 16; i++) {
            if (gEeprom.correctionTable.temperature[i] == 0x7FFF)
                break;

            iMax = i;
        }

        int16_t valCor = 0;

        switch (iMax) {
        case 0xFF:
            valCor = 0;
            break;
        case 0x00: {
                valCor += gEeprom.correctionTable.correction[0];
                break;
            }
        default: {
                if (sensorTmp[sensorIdx] < gEeprom.correctionTable.temperature[0]) {
                    valCor += gEeprom.correctionTable.correction[0];
                    break;
                }

                if (sensorTmp[sensorIdx] >= gEeprom.correctionTable.temperature[iMax]) {
                    valCor += gEeprom.correctionTable.correction[iMax];
                    break;
                }

                for (uint8_t i = 0; i < iMax; i++) {
                    if ((sensorTmp[sensorIdx] >= gEeprom.correctionTable.temperature[i]) && (sensorTmp[sensorIdx] < gEeprom.correctionTable.temperature[i + 1])) {
                        int32_t t1 = gEeprom.correctionTable.temperature[i];
                        int32_t t2 = gEeprom.correctionTable.temperature[i + 1];
                        int32_t p1 = gEeprom.correctionTable.correction[i] * 10;
                        int32_t p2 = gEeprom.correctionTable.correction[i + 1] * 10;

                        valCor += p1 + (sensorTmp[sensorIdx] - t1) * (p2 - p1) / (t2 - t1);
                        if (valCor % 10 >= 5)
                            valCor = valCor / 10 + 1;
                        else
                            valCor = valCor / 10;

                        break;
                    }
                }
            }
            break;
        }

        sensorCor[sensorIdx] = valCor;

        // -------------------------------------------------------------------------------------------------------------
        // Усреднение

        if (sensorAvgCnt < gEeprom.measure.sampleSize)
            sensorAvgCnt++;

        sensorAvg = 0;
        for (uint8_t i = 0; i < sensorAvgCnt; i++)
            sensorAvg += sensorTmp[i] + sensorCor[i];

        sensorAvg /= sensorAvgCnt;

        taskENTER_CRITICAL();
        gTmpSensor    = sensorTmp[sensorIdx];
        gTmpCorrected = sensorTmp[sensorIdx] + sensorCor[sensorIdx];
        gTmpAverage   = sensorAvg;
        gTemperatureLog.append(gTmpSensor, gTmpCorrected, gTmpAverage);
        taskEXIT_CRITICAL();

        if (++sensorIdx >= gEeprom.measure.sampleSize)
            sensorIdx = 0;

        //stAdt = uxTaskGetStackHighWaterMark(NULL);
    }
}

extern "C" __irq void EXTI2_3_IRQHandler()
{
    //GPIOA->ODR &= ~GPIO_ODR_OD1;
    //LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_1);
    //uptimeSave();
    //while (1);
}

extern "C" __irq void TIM21_IRQHandler()
{
    if (RCC->CIFR & RCC_CIFR_LSECSSF) {
        RCC->CICR |= RCC_CICR_LSECSSC;
        gInit = -1;

        TIM21->CCER &= ~TIM_CCER_CC1E;
        SRF(RCC->ICSCR, RCC_ICSCR_MSITRIM, 0);

        PWR->CR |= PWR_CR_DBP;
        RCC->CSR |= RCC_CSR_RTCRST;
        RCC->CSR &= ~RCC_CSR_RTCRST;
        RCC->CSR |= RCC_CSR_LSEON;
        return;
    }

    TIM21->SR = 0;

    static uint16_t capt[2];
    static uint16_t captDiff;

    static uint8_t preDelay;
    static uint8_t postDelay;

    switch (gInit) {
    case 0: {
            if (preDelay++ < 100)
                break;

            capt[gInit++] = TIM21->CCR1;
        }
        break;

    case 1: {
            capt[gInit++] = TIM21->CCR1;
        }
        break;

    case 2: {
            if (postDelay++ < 100)
                break;

            gInit = -1;

            captDiff = capt[1] - capt[0];

            if (captDiff > 512) {
                if ((GRF(RCC->ICSCR, RCC_ICSCR_MSICAL) & 0x3F) == 0)
                    SRF(RCC->ICSCR, RCC_ICSCR_MSITRIM, (GRF(RCC->ICSCR, RCC_ICSCR_MSITRIM) - 0x1C));
                else
                    SRF(RCC->ICSCR, RCC_ICSCR_MSITRIM, (GRF(RCC->ICSCR, RCC_ICSCR_MSITRIM) - 1));
            }

            if (captDiff < 512) {
                if ((GRF(RCC->ICSCR, RCC_ICSCR_MSICAL) & 0x3F) == 0x3F)
                    SRF(RCC->ICSCR, RCC_ICSCR_MSITRIM, (GRF(RCC->ICSCR, RCC_ICSCR_MSITRIM) + 0x1A));
                else
                    SRF(RCC->ICSCR, RCC_ICSCR_MSITRIM, (GRF(RCC->ICSCR, RCC_ICSCR_MSITRIM) + 1));
            }
        }
        break;

    case 0xFF: {
            preDelay = 0;
            postDelay = 0;
            gInit = 0;
        }
        break;
    }
}

extern "C" __irq void RCC_IRQHandler()
{
    if (RCC->CIFR & RCC_CIFR_LSERDYF) {
        RCC->CICR |= RCC_CICR_LSERDYC;
        gInit = -1;

        RCC->CSR |= RCC_CSR_LSECSSON;
        PWR->CR &= ~PWR_CR_DBP;
        TIM21->CCER |= TIM_CCER_CC1E;
    }

    if (RCC->CIFR & RCC_CIFR_LSECSSF)
        RCC->CICR |= RCC_CICR_LSECSSC;
}

void main(void)
{
    SystemInit();

    RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;

    NVIC_SetPriority(SVC_IRQn, 0);
    NVIC_SetPriority(PendSV_IRQn, 0);
    NVIC_SetPriority(SysTick_IRQn, 0);

    SysTick->LOAD = (uint32_t)((2097152 / 1000) - 1UL);
    SysTick->VAL  = 0;
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
    NVIC_SetPriority(SysTick_IRQn, 0);

    RCC->IOPENR |= RCC_IOPENR_GPIOAEN;

    //Init_IWDG(1200);
    //IWDG_res();
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    RCC->APB2ENR |= RCC_APB2ENR_TIM21EN;
    RCC->IOPENR |= RCC_IOPENR_IOPAEN;
    RCC->AHBENR |= RCC_AHBENR_DMAEN;

    // Включение стабилизатора 3.0В для датчика температуры
    SRF(GPIOA->MODER, GPIO_MODER_MODE1, 1);
    GPIOA->ODR |= GPIO_ODR_OD1;

    // Сигнал от источника питания
    SRF(GPIOA->MODER, GPIO_MODER_MODE2, 0);
    SRF(GPIOA->PUPDR, GPIO_PUPDR_PUPD2, 1);
    EXTI->IMR |= EXTI_IMR_IM2;
    EXTI->FTSR |= EXTI_FTSR_FT2;
    //NVIC_EnableIRQ(EXTI2_3_IRQn);

    // MCO
    GPIOA->MODER &= ~(GPIO_MODER_MODE8);
    GPIOA->MODER |= GPIO_MODER_MODE8_1;
    RCC->CFGR &= ~(RCC_CFGR_MCOPRE | RCC_CFGR_MCOSEL);
    RCC->CFGR |= RCC_CFGR_MCOPRE_2 | RCC_CFGR_MCOSEL_SYSCLK;

    // RCC LSE with CSS
    PWR->CR |= PWR_CR_DBP;
    RCC->CSR |= RCC_CSR_RTCRST;
    RCC->CSR &= ~RCC_CSR_RTCRST;
    RCC->CIER |= RCC_CIER_LSERDYIE | RCC_CIER_LSECSSIE;
    NVIC_EnableIRQ(RCC_IRQn);
    RCC->CSR |= RCC_CSR_LSION | RCC_CSR_LSEON;

    // TIM21
    TIM21->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_IC1PSC_0 | TIM_CCMR1_IC1PSC_1;
    TIM21->DIER = TIM_DIER_CC1IE;
    TIM21->OR = TIM21_OR_TI1_RMP_2;
    NVIC_EnableIRQ(TIM21_IRQn);
    TIM21->CR1 = TIM_CR1_CEN;

    if (!nvmEepromLoadCheckCrc(&gEeprom.modbus, (int32_t)&gEeprom.modbus - (int32_t)&gEeprom, sizeof(gEeprom.modbus)))
        gStatus0.errLoadConnection = 1;

    if (!nvmEepromLoadCheckCrc(&gEeprom.measure, (int32_t)&gEeprom.measure - (int32_t)&gEeprom, sizeof(gEeprom.measure)))
        gStatus0.errLoadMeasure = 1;

    if (!nvmEepromLoadCheckCrc(&gEeprom.dateManufacture, (int32_t)&gEeprom.dateManufacture - (int32_t)&gEeprom, sizeof(gEeprom.dateManufacture)))
        gStatus0.errLoadDateManufacture = 1;

    if (!nvmEepromLoadCheckCrc(&gEeprom.dateVerification, (int32_t)&gEeprom.dateVerification - (int32_t)&gEeprom, sizeof(gEeprom.dateVerification)))
        gStatus0.errLoadDateVerification = 1;

    if (!nvmEepromLoadCheckCrc(&gEeprom.serial, (int32_t)&gEeprom.serial - (int32_t)&gEeprom, sizeof(gEeprom.serial)))
        gStatus0.errLoadSerial = 1;

    if (!nvmEepromLoadCheckCrc(&gEeprom.correctionTable, (int32_t)&gEeprom.correctionTable - (int32_t)&gEeprom, sizeof(gEeprom.correctionTable)))
        gStatus0.errLoadCorrectionTable = 1;

    __disable_irq();
    uptimeInit();
    __enable_irq();

    xTaskCreateStatic(taskUptimeAdc, "uptime_adc", stackSizeAdc, NULL, 3, stackAdc, &tcbAdc);
    xTaskCreateStatic(taskCommunication, "modbus", stackSizeModbus, NULL, 1, stackModbus, &tcbModbus);
    xTaskCreateStatic(taskSensor, "tmp116", stackSizeTmp, NULL, 2, stackTmp, &tcbTmp);
    vTaskStartScheduler();

    while (1);
}

/*
extern "C" void vApplicationIdleHook()
{
    //__WFI();
}
*/

extern "C" void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
        StackType_t **ppxIdleTaskStackBuffer,
        uint32_t *pulIdleTaskStackSize)
{
    /* If the buffers to be provided to the Idle task are declared inside this
    function then they must be declared static - otherwise they will be allocated on
    the stack and so not exists after this function exits. */
    static StaticTask_t xIdleTaskTCB;
    static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

    /* Pass out a pointer to the StaticTask_t structure in which the Idle task's
    state will be stored. */
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

    /* Pass out the array that will be used as the Idle task's stack. */
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;

    /* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
    Note that, as the array is necessarily of type StackType_t,
    configMINIMAL_STACK_SIZE is specified in words, not bytes. */
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

