#define _HOLDINGS_CPP

#include "modbus.h"
#include "globals.h"
#include "stm32l051xx.h"
#include "globals.h"
#include "intrinsics.h"

uint16_t HOLDING_REGION[][2] = {
    {0x0000, 0x0002},
    {0x0010, 0x0011},
    {0xFFFF, 0xFFFF}
};

__HOLDING_DEFINE_RANGE(0x0000, 1, 247);
__HOLDING_DEFINE_RANGE(0x0001, 0, 9);
__HOLDING_DEFINE_RANGE(0x0002, 0, 2);

__HOLDING_DEFINE_RANGE(0x0010, 1, 10);
__HOLDING_DEFINE_RANGE(0x0011, 1, 10);

eMBException extModbusHoldings(uint16_t *pRegBuffer, uint16_t address, uint8_t count, eMBOperation operation)
{
    if (checkRegsRegion(HOLDING_REGION, address, count) == false)
        return MB_EX_ILLEGAL_DATA_ADDRESS;

    uint16_t __packed *pRegs = pRegBuffer;

    if (operation == MB_OP_READ) {
        switch (address) {
        case 0x0000: // Напряжение входного питания
            *pRegs++ = __REVSH(gEeprom.modbus.address);
            if (--count == 0)
                break;
        case 0x0001:
            *pRegs++ = __REVSH(gEeprom.modbus.baudrate);
            if (--count == 0)
                break;
        case 0x0002:
            *pRegs++ = __REVSH(gEeprom.modbus.parity);
            break;

        case 0x00010:
            *pRegs++ = __REVSH(gEeprom.measure.period);
            if (--count == 0)
                break;
        case 0x0011:
            *pRegs++ = __REVSH(gEeprom.measure.sampleSize);
            break;
        }

        return MB_EX_NONE;
    }

    if (operation == MB_OP_WRITE) {
        bool fModbus   = false;
        bool fMeasure  = false;
        int  value;

        sModbusConfig  newMb = gEeprom.modbus;
        sMeasureConfig newMeas = gEeprom.measure;

        switch (address) {
        case 0x0000: // Напряжение входного питания
            value = __REVSH(*pRegs++);
            if (!__HOLDING_CHECK(0x0000, 0, value))
                return MB_EX_ILLEGAL_DATA_VALUE;
            newMb.address = value;
            fModbus = true;
            if (--count == 0)
                break;
        case 0x0001:
            value = __REVSH(*pRegs++);
            if (!__HOLDING_CHECK(0x0001, 0, value))
                return MB_EX_ILLEGAL_DATA_VALUE;
            newMb.baudrate = value;
            fModbus = true;
            if (--count == 0)
                break;
        case 0x0002:
            value = __REVSH(*pRegs++);
            if (!__HOLDING_CHECK(0x0002, 0, value))
                return MB_EX_ILLEGAL_DATA_VALUE;
            newMb.parity = value;
            fModbus = true;
            break;

        case 0x0010:
            value = __REVSH(*pRegs++);
            if (!__HOLDING_CHECK(0x0010, 0, value))
                return MB_EX_ILLEGAL_DATA_VALUE;
            newMeas.period = value;
            fMeasure = true;
            if (--count == 0)
                break;

        case 0x0011:
            value = __REVSH(*pRegs++);
            if (!__HOLDING_CHECK(0x0011, 0, value))
                return MB_EX_ILLEGAL_DATA_VALUE;
            newMeas.sampleSize = value;
            fMeasure = true;
            break;
        }

        if (fModbus) {
            gEeprom.modbus = newMb;
            gEeprom.modbus.crc = crc16(&gEeprom.modbus, sizeof(gEeprom.modbus) - 2);

            if (nvmEepromWrite((uint32_t)&gEeprom.modbus - (uint32_t)&gEeprom, &gEeprom.modbus, sizeof(gEeprom.modbus)) != NVM_OK) {
                gStatus0.errLoadConnection = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadConnection = 0;
        }

        if (fMeasure) {
            gEeprom.measure = newMeas;
            gEeprom.measure.crc = crc16(&gEeprom.measure, sizeof(gEeprom.measure) - 2);
            if (nvmEepromWrite((uint32_t)&gEeprom.measure - (uint32_t)&gEeprom, &gEeprom.measure, sizeof(gEeprom.measure)) != NVM_OK) {
                gStatus0.errLoadMeasure = 1;
                return (eMBException)MDB_EX_EEPROM_WRITE;
            }
            gStatus0.errLoadMeasure = 0;
        }

        return MB_EX_NONE;
    }

    return MB_EX_SLAVE_DEVICE_FAILURE;
}

bool __holdingRegisterCheck(uint16_t const *_pointer, bool _signed, uint16_t _value)
{
    if (_pointer[0]) {
        for (uint8_t i = 0; i < _pointer[1]; i++)
            if (_pointer[2 + i] == _value)
                return true;

        return false;
    }

    if (_signed) {
        if ((int16_t)_pointer[1] <= (int16_t)_value && (int16_t)_value <= (int16_t)_pointer[2])
            return true;
    } else {
        if (_pointer[1] <= _value && _value <= _pointer[2])
            return true;
    }

    return false;
}

